from django import template
from django.utils.translation import get_language

register = template.Library()


@register.simple_tag
def get_url_tag(request, lang):
    if lang:
        active_language = get_language()
        return request.path.replace(active_language, lang, 1)
    return request.path


@register.simple_tag
def get_birbalo_item(c_data, p1, p2, p3):
    key = "{}_{}".format(p1, p2)
    p3 = int(p3)
    if key not in c_data:
        return 0

    return c_data[key][p3]

