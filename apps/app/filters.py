import django_filters
from apps.app import models as app_models


class DepartmentFilters(django_filters.FilterSet):
    class Meta:
        model = app_models.Departments
        fields = ('id', 'inn')
