from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
import csv
from apps.app import models as app_models
from apps.app import forms as app_forms
from datetime import datetime
from django.views import generic

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger



class RegionsListView(generic.ListView):
    template_name = 'account/regions/regions.html'
    model = app_models.Regions

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['regions'] = app_models.Regions.objects.order_by('id')
        return context


def regions_view(request):
    regions = app_models.Regions.objects.all()
    context = {'regions': regions}

    return render(request, 'account/regions/regions.html', context)


class RegionCreateView(generic.CreateView):
    def get(self, request, *args, **kwargs):
        context = {'forms': app_forms.RegionsForm()}
        return render(request, 'account/regions/create_region.html', context=context)

    def post(self, request, *args, **kwargs):
        form = app_forms.RegionsForm(request.POST)
        # print(form)
        if form.is_valid():
            print(form)
            region = form.save()
            region.created_at = datetime.now().strftime("%Y-%m-%d")
            region.save()
            return HttpResponseRedirect(reverse_lazy('regions-page'))
        return render(request, 'account/regions/create_region.html', {'forms': form})


class RegionUpdateView(generic.UpdateView):
    form_class = app_forms.RegionsForm
    template_name = 'account/regions/update_region.html'
    queryset = app_models.Regions.objects.all()

    def get_object(self, queryset=None):
        id_ = self.kwargs.get('id')
        return get_object_or_404(app_models.Regions, id=id_)

    def form_valid(self, form):
        data = form.save(commit=False)
        data.updated_at = datetime.now().strftime("%Y-%m-%d")
        data.save()

        return super(RegionUpdateView, self).form_valid(form)


def delete_region(request, id):
    regions = app_models.Regions.objects.filter(id=id)

    if request.method == 'POST':
        regions.delete()
        return redirect('regions-page')

    context = {'regions': regions, 'id': id}
    return render(request, 'account/regions/delete_region.html', context)


class DepartmentListView(generic.ListView):
    template_name = 'departments/departments.html'
    model = app_models.Departments
    paginate_by = 20

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['departments'] = app_models.Departments.objects.order_by('id')
        list_exam = app_models.Departments.objects.order_by('id')
        paginator = Paginator(list_exam, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            file_exams = paginator.page(page)
        except PageNotAnInteger:
            file_exams = paginator.page(1)
        except EmptyPage:
            file_exams = paginator.page(paginator.num_pages)

        context['list_exams'] = file_exams
        return context


class DepartmentCreateView(generic.CreateView):

    def get_context_data(self, **kwargs):
        context = super(DepartmentCreateView, self).get_context_data(**kwargs)
        # context['filter'] = DepartmentFilters(self.request.GET, queryset=self.get_queryset())
        return context

    def get(self, request, *args, **kwargs):
        context = {'forms': app_forms.DepartmentForm()}
        return render(request, 'departments/create_department.html', context=context)

    def post(self, request, *args, **kwargs):
        form = app_forms.DepartmentForm(request.POST)
        if form.is_valid():
            department = form.save()
            department.created_at = datetime.now().strftime("%Y-%m-%d")
            department.save()
            return HttpResponseRedirect(reverse_lazy('departments'))
        return render(request, 'departments/create_department.html', {'form': app_forms.DepartmentForm()})


class CreateDepartmentType(generic.CreateView):
    def get(self, request, *args, **kwargs):
        context = {"forms": app_forms.DepartmentTypeForm()}
        return render(request, 'departments/create_department_type.html', context)

    def post(self, request, *args, **kwargs):
        form = app_forms.DepartmentTypeForm(request.POST)
        if form.is_valid():
            department_type = form.save(commit=False)
            department_type.created_at = datetime.now().strftime("%Y-%m-%d")
            department_type.save()
            return HttpResponseRedirect(reverse_lazy('create-department'))
        return render(request, 'departments/create_department_type.html', {'form': form})


class DepartmentUpdateView(generic.UpdateView):
    template_name = 'departments/update_department.html'
    form_class = app_forms.DepartmentForm
    # queryset = app_models.Departments

    def get_object(self, queryset=None):
        id_ = self.kwargs.get('id')
        return get_object_or_404(app_models.Departments, id=id_)

    def form_valid(self, form):
        data = form.save(commit=False)
        data.updated_at = datetime.now().strftime("%Y-%m-%d")
        data.save()
        return redirect('departments')


def delete_department(request, id):
    department = app_models.Departments.objects.filter(id=id)

    if request.method == 'POST':
        department.delete()
        return redirect('departments')

    context = {'regions': department, 'id': id}
    return render(request, 'departments/delete_department.html', context)


def control_data_view(request):
    context = {'controlData': app_models.ControlDataFirst.objects.order_by('-id')}
    return render(request, 'control_data_table/data_table.html', context)


class CreateConData(generic.TemplateView):
    template_name = 'control_data_table/create_con_data.html'

    def get(self, *args, **kwargs):
        formset = app_forms.ControlDataItemFormset(queryset=app_models.ControlDateFirstItem.objects.none())
        first_form = app_forms.ControlDataFirstForm()
        return render(self.request, self.template_name, context={'formset': formset, 'first_form': first_form})

    # Define method to handle POST request
    def post(self, *args, **kwargs):
        main_data = None
        if "period_start" in self.request.POST:
            first_form = app_forms.ControlDataFirstForm(data=self.request.POST)
            year = first_form['year'].value()
            month = first_form['month'].value()
            if first_form.is_valid():
                main_data = first_form.save(commit=False)
                main_data.control_date = str(month) + " " + str(year)
                main_data.save()
        if "form-TOTAL_FORMS" in self.request.POST:
            formset = app_forms.ControlDataItemFormset(data=self.request.POST)

            # Check if submitted forms are valid
            if formset.is_valid():
                print('formset', formset.data)
                many_data = formset.save()
                if main_data:
                    for i in many_data:
                        i.control_date_first = main_data
                        main_data.condition_count = main_data.condition_count + getattr(i, 'condition_count')
                        main_data.save()
                        main_data.condition_summa = main_data.condition_summa + getattr(i, 'condition_summa')
                        main_data.save()
                        i.save()
                return redirect(reverse_lazy("control-data-table"))

        return self.render_to_response({'bird_formset': 32})


def delete_con_data(request, id):
    data = app_models.ControlDataFirst.objects.filter(id=id)

    if request.method == 'POST':
        data.delete()
        return redirect('control-data-table')

    context = {'data': data, 'id': id}
    return render(request, 'control_data_table/delete_con_data.html', context)


def control_data_detail_view(request, id):
    control_data = app_models.ControlDataFirst.objects.filter(id=id)
    activities = []
    first_items = app_models.ControlDateFirstItem.objects.filter(control_date_first_id=id)
    for i in first_items:
        activities.append(i.control_activate_type.name_uz)

    context = {'control_data': control_data, 'id': id, 'first_items':first_items, 'activities':activities}
    return render(request, 'control_data_table/detail_data.html', context)


class ControlDataTableView(generic.TemplateView):
    template_name = 'report/control_data.html'
    model = app_models.ControlDateFirstItem.objects.order_by("-id")

    def get_context_data(self, **kwargs):
        context = super(ControlDataTableView, self).get_context_data(**kwargs)
        first_items = app_models.ControlDateFirstItem.objects.all()
        context['activities'] = app_models.ControlActivitiesType.objects.all()[:11]
        context['controlData'] = app_models.ControlDataFirst.objects.order_by('-id')
        context['items'] = first_items
        con_data_first = app_models.ControlDataFirst.objects.order_by("-id")
        c_data = {}

        for row in app_models.ControlDateFirstItem.objects.filter(
                control_date_first_id__in=[k.id for k in con_data_first]).all():
            key = "{}_{}".format(row.control_date_first_id, row.control_activate_type_id)
            c_data[key] = [row.condition_count, row.condition_summa]

        context['activities'] = app_models.ControlActivitiesType.objects.all()[:11]
        context['controlData'] = con_data_first
        context["con_data"] = c_data
        return context


def control_activity(request):
    activities = app_models.ControlActivitiesType.objects.all()
    context = {'activities': activities}
    return render(request, 'control_activities/control_activities.html', context)


class ActivityCreateView(generic.CreateView):
    def get(self, request, *args, **kwargs):
        context = {'activities': app_forms.ControlActivityForm()}
        return render(request, 'control_activities/create_activity.html', context)

    def post(self, request, *args, **kwargs):
        form = app_forms.ControlActivityForm(request.POST)
        if form.is_valid():
            activity = form.save(commit=False)
            activity.created_at = datetime.now().strftime("%Y-%m-%d")
            activity.save()
            return HttpResponseRedirect(reverse_lazy('control-activities'))
        return render(request, 'control_activities/create_activity.html',
                      {'activities': app_forms.ControlActivityForm()})


class ActivityUpdateView(generic.UpdateView):
    template_name = 'control_activities/update_activity.html'
    form_class = app_forms.ControlActivityForm

    def get_object(self, queryset=None):
        id_ = self.kwargs.get('id')
        return get_object_or_404(app_models.ControlActivitiesType, id=id_)

    def form_valid(self, form):
        data = form.save(commit=False)
        data.updated_at = datetime.now().strftime("%Y-%m-%d")
        data.save()
        return redirect('control-activities')


def delete_activity(request, id):
    activity = app_models.ControlActivitiesType.objects.filter(id=id)

    if request.method == 'POST':
        activity.delete()
        return redirect('control-activities')

    context = {'activities': activity, 'id': id}
    return render(request, 'control_activities/delete_activity.html', context)
