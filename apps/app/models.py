from django.contrib.humanize.templatetags.humanize import intcomma
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Sum
from django.template.defaultfilters import floatformat
from django.urls import reverse
from mptt.models import MPTTModel
from mptt.fields import TreeForeignKey

import calendar

from django.utils.translation import gettext_lazy as _


class Regions(MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True,
                            related_name='children', verbose_name=_('parent'))
    name_uz = models.CharField(max_length=100, verbose_name=_('name uz'))
    name_ru = models.CharField(max_length=100, verbose_name=_('name ru'))
    soato_code = models.IntegerField(null=True, blank=True, verbose_name=_('sato code'))
    created_at = models.DateField(null=True, blank=True, verbose_name=_('create at'))
    updated_at = models.DateField(null=True, blank=True, verbose_name=_('update at'))
    is_city = models.BooleanField(null=True, blank=True, default=False, verbose_name=_('is city'))

    def get_absolute_url(self):
        return reverse('regions-page')

    def __str__(self):
        return self.name_uz


class DepartmentType(models.Model):
    name_uz = models.CharField(max_length=200, verbose_name=_('name uz'))
    name_ru = models.CharField(max_length=200, verbose_name=_('name ru'))
    created_at = models.DateField(auto_now=True, verbose_name=_('created at'))
    updated_at = models.DateField(auto_now=True, verbose_name=_('update at'))

    def __str__(self):
        return self.name_uz


class Departments(MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True,
                            related_name='children', verbose_name=_('parent'))
    department_type = models.ForeignKey(DepartmentType, on_delete=models.CASCADE, null=True, blank=True,
                                        related_name='department_type', verbose_name=_('department type'))
    inn = models.IntegerField(default=0)
    name_uz = models.CharField(max_length=200, verbose_name=_("name uz"))
    name_ru = models.CharField(max_length=200, verbose_name=_('name ru'))
    created_at = models.DateField(auto_now=True, verbose_name=_('created at'))
    updated_at = models.DateField(auto_now=True, verbose_name=_('updated at'))

    def __str__(self):
        return "{}".format(str(self.inn))

    def get_absolute_url(self):
        return reverse('departments')


class ControlActivitiesType(models.Model):
    name_uz = models.CharField(max_length=200, verbose_name=_('name uz'))
    name_ru = models.CharField(max_length=200, verbose_name=_('name ru'))
    created_at = models.DateField(auto_now=True, verbose_name=_('created at'))
    updated_at = models.DateField(auto_now=True, verbose_name=_("updated at"))

    def __str__(self):
        return self.name_uz


class ControlDataFirst(models.Model):
    department = models.ForeignKey(Departments, on_delete=models.CASCADE, null=True, blank=True, verbose_name=_('department'))
    region = models.ForeignKey(Regions, on_delete=models.CASCADE, null=True, blank=True, verbose_name=_('region'))
    control_date = models.CharField(max_length=200, null=True, blank=True, verbose_name=_('control date'))
    period_start = models.DateTimeField(null=True, blank=True, verbose_name=_('period start'))
    period_end = models.DateTimeField(null=True, blank=True, verbose_name=_('period end'))
    condition_count = models.IntegerField(null=True, blank=True, default=0, verbose_name=_('condition count'))
    condition_summa = models.FloatField(null=True, blank=True, default=0, verbose_name=_('condition summa'))
    created_at = models.DateField(auto_now=True, verbose_name=_('created at'))
    updated_at = models.DateField(null=True, blank=True, verbose_name=_('update at'))

    def get_items(self):
        return self.con_data_item.filter(id=self.id)


class ControlDateFirstItem(models.Model):
    control_date_first = models.ForeignKey(ControlDataFirst, on_delete=models.CASCADE, null=True, blank=True,
                                           related_name='con_data_item', verbose_name=_("control date first"))
    control_activate_type = models.ForeignKey(ControlActivitiesType,
                                              on_delete=models.CASCADE, null=True, blank=True,
                                              related_name='con_data_item', verbose_name=_("control activate type"))
    condition_count = models.IntegerField(null=True, blank=True, verbose_name=_("condition count"))
    condition_summa = models.FloatField(null=True, blank=True, verbose_name=_("condition summa"))

