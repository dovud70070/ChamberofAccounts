from django import forms
from apps.app import models as app_models
from django.forms import modelformset_factory
import datetime

class RegionsForm(forms.ModelForm):
    YES = True
    NO = False
    IS_CITY = (
        (YES, 'Yes'),
        (NO, 'No'),
    )
    is_city = forms.ChoiceField(widget=forms.Select({
        'class': 'rounded'
    }), choices=IS_CITY, initial=False, label='Is it city?')

    class Meta:
        model = app_models.Regions
        fields = "__all__"

        widgets = {
            'name_uz': forms.TextInput({
                'placeholder': '',
                'class': 'form-control rounded',
                'required': True,
                'maxlength': 30,
            }),

            'name_ru': forms.TextInput({
                'placeholder': '',
                'class': 'form-control rounded',
                'required': True,
                'maxlength': 30,
            }),
            'parent_id': forms.NumberInput({
                'placeholder': 'number',
                'class': 'rounded',
                'required': False,
                'maxlength': 10
            }),
            'soato_code': forms.NumberInput({
                'placeholder': '',
                'class': 'form-control rounded',
                'required': False,
                'maxlength': 10,
            }),
            'created_at': forms.HiddenInput({
                'placeholder': '',
                'class': 'rounded',
                'required': True,
                'maxlength': 10,
            }),
            'updated_at': forms.HiddenInput({
                'placeholder': '',
                'class': 'rounded',
                'required': True,
                'maxlength': 10,

            }),
            'parent': forms.Select({
                'placeholder': '',
                'class': 'rounded',
                'required': False,

            })
        }

        labels = {
            'parent': 'shahar/viloyat id raqami',
            'name_uz': 'shahar yoki viloyat/tuman nomi',
            'name_ru': 'название города или провинции/района',
            'is_city': 'bu shaharmi?'
        }


class DepartmentForm(forms.ModelForm):
    class Meta:
        model = app_models.Departments
        fields = "__all__"
        widgets = {
            'name_uz': forms.TextInput({
                'placeholder': '',
                'class': 'rounded',
                'required': True,
                'maxlength': 30,
            }),
            'name_ru': forms.TextInput({
                'placeholder': '',
                'class': 'rounded',
                'required': True,
                'maxlength': 30,
            }),
            'created_at': forms.HiddenInput({
                'placeholder': '',
                'class': 'rounded',
                'required': False,
                'maxlength': 10,
            }),
            'updated_at': forms.HiddenInput({
                'placeholder': '',
                'class': 'rounded',
                'required': False,
                'maxlength': 10,

            }),
            'parent': forms.Select({
                'class': 'rounded'
            }),
            'department_type': forms.Select({
                'class': 'rounded',
                'required': False,
            }),
            'inn': forms.NumberInput({
                'required': False
            })

        }

        labels = {
            'name_uz': "bo'lim",
            'name_ru': 'раздел',
            'department_type': "bo'lim turi"
        }


class DepartmentTypeForm(forms.ModelForm):
    class Meta:
        model = app_models.DepartmentType
        fields = "__all__"
        widgets = {
            'name_uz': forms.TextInput({
                'placeholder': '',
                'class': 'rounded',
                'required': True,
                'maxlength': 30,
            }),
            'name_ru': forms.TextInput({
                'placeholder': '',
                'class': 'rounded',
                'required': True,
                'maxlength': 30,
            }),
            'created_at': forms.HiddenInput({
                'placeholder': '',
                'class': 'rounded',
                'required': True,
                'maxlength': 10,
            }),
            'updated_at': forms.HiddenInput({
                'placeholder': '',
                'class': 'rounded',
                'required': True,
                'maxlength': 10,

            }),
        }

def valid_year(year):
    if year and year.isdigit():
        if int(year) in (1900, 2900):
            return int(year)
    return 'Year is not valid!!!'

class ControlDataFirstForm(forms.ModelForm):
    JANUARY = 'Yanvar'
    FEBRUARY = 'Fevral'
    MARCH = 'Mart'
    APRIL = 'Aprel'
    MAY = 'May'
    JUNE = 'Iyun'
    JULY = 'Iyul'
    AUGUST = 'Avgust'
    SEPTEMBER = 'Sentyabr'
    OCTOBER = 'Oktyabr'
    NOVEMBER = 'Noyabr'
    DECEMBER = 'Dekabr'

    MONTH_CHOICES = ((JANUARY, 'Yanvar'), (FEBRUARY, 'Fevral'), (MARCH, 'Mart'), (APRIL, 'Aprel'), (MAY, 'May'), (JUNE, 'Iyun'), (JULY, 'Iyul'), (AUGUST, 'Avgust'), (SEPTEMBER, 'Sentyabr'), (OCTOBER, 'Oktyabr'), (NOVEMBER, 'Noyabr'), (DECEMBER, 'Dekabr'))
    YEAR_CHOICES = [(f, '{}'.format(f)) for f in range(datetime.date.today().year-10, datetime.date.today().year+10)]
    month = forms.CharField(required=True, label='oy', widget=forms.Select(choices=MONTH_CHOICES, attrs={'class':'rounded'}))
    year = forms.CharField(required=True, label='yil', widget=forms.Select(choices=YEAR_CHOICES, attrs={'class':'rounded'}), initial=datetime.date.today().year)

    class Meta:
        model = app_models.ControlDataFirst
        fields = ['department', 'region', 'control_date', 'period_start', 'period_end', 'condition_count', 'condition_summa']  #'department', 'region',

        widgets = {
            'period_start': forms.DateTimeInput(
                attrs={
                    'class': 'date-picker form-control rounded ',
                    'placeholder': "mm/dd/yyyy",
                    'type': "text",
                    'required': "required",
                    'onfocus': "this.type='date'",
                    'onmouseover': "this.type='date'",
                    'onclick': "this.type='date'",
                    'onblur': "this.type='text'",
                    'onmouseout': "timeFunctionLong(this)"
                }
            ),
            'period_end': forms.DateTimeInput(
                attrs={
                    'class': 'date-picker form-control rounded',
                    'placeholder': "mm/dd/yyyy",
                    'type': "text",
                    'required': "required",
                    'onfocus': "this.type='date'",
                    'onmouseover': "this.type='date'",
                    'onclick': "this.type='date'",
                    'onblur': "this.type='text'",
                    'onmouseout': "timeFunctionLong(this)"
                }
            ),
            'region': forms.Select(attrs={
                'class': 'rounded region',
                'id': 'region',
            }, ),
            'department': forms.Select(attrs={
                'class': 'rounded department',
                'id': 'department',
            }),
            'condition_count': forms.TextInput(attrs={
                'class': 'rounded',
                'disabled': 'disabled',
                'id': 'data_count'
            }),
            'condition_summa': forms.TextInput(attrs={
                'class': 'rounded',
                'disabled': 'disabled',
                'id': 'data_summa'
            })
        }
        labels = {
            'period_start': ':dan',
            'period_end': ':gacha',
            'control_date': 'Tadbir oyi',
            'department': "Tashkilot",
            'region': "Tadbir o'tkaziladigan joy",
            'condition_count':'Soni',
            'condition_summa':'Summasi'

        }


class ControlDataFirstItemForm(forms.ModelForm):
    class Meta:
        model = app_models.ControlDateFirstItem
        fields = "__all__"
        widgets = {
            'control_activate_type':forms.Select(attrs={
                'class':'rounded focus-input100',
            }),
            'condition_count':forms.NumberInput(attrs={
                'class':'rounded condition_count_value',
                'id':'condition_count_value'
            }),
            'condition_summa': forms.NumberInput(attrs={
                'class': 'rounded condition_summa_value',
                'id':'condition_summa_value'
            })
        }
        labels = {
            'control_activate_type': 'Tadbir nomi',
            'condition_count': 'Soni',
            'condition_summa': 'Summasi'
        }


ControlDataItemFormset = modelformset_factory(
    app_models.ControlDateFirstItem, fields=('control_activate_type', 'condition_count', 'condition_summa'), extra=1, form=ControlDataFirstItemForm
)

class ControlActivityForm(forms.ModelForm):
    class Meta:
        model = app_models.ControlActivitiesType
        fields = '__all__'