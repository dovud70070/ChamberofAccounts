import xlwt
from apps.app import models as app_models
from django.http import HttpResponseRedirect, HttpResponse
import csv


def excel_view(request):
    wb = xlwt.Workbook()
    ws = wb.add_sheet('Order')
    lefted_style = xlwt.easyxf('align: wrap on, vert center, horiz left;')
    centered_style = xlwt.easyxf('align: wrap on, vert center, horiz center;')

    zero_col = ws.col(0)
    zero_col.width = 1040

    fir_col = ws.col(1)
    fir_col.width = 1000

    sec_col = ws.col(2)
    sec_col.width = 12040

    fifth_col = ws.col(5)
    fifth_col.width = 3040
    centered_green_style = xlwt.easyxf('align: wrap on, vert center, horiz center; pattern: pattern solid, '
                                       'fore_colour green; font: color white;')
    invoice_style = xlwt.easyxf('font: bold on, height 280;')
    ws.write(0, 0, 'T/r', style=centered_style)
    ws.write_merge(1, 1, 0, 13, ' ', style=centered_style)
    ws.write_merge(0, 0, 2, 2, "Tashkilot nomi", style=centered_style)
    ws.write_merge(0, 0, 3, 4, "Joylashgan joyi", style=centered_style)
    ws.write_merge(0, 0, 5, 6, "Vazirlik tizimiga mansubligi", style=centered_style)
    ws.write_merge(0, 0, 7, 9, "Nazorat tadbiri o'tkazilgan oy", style=centered_style)
    ws.write_merge(0, 0, 10, 11, "Qamrab olingan davr boshi", style=centered_style)
    ws.write_merge(0, 0, 12, 13, "Qamrab olingan Davr Ohiri", style=centered_style)
    ws.write_merge(0, 0, 14, 15, "Jami", style=centered_style)
    ws.write(1, 14, "soni", style=centered_style)
    ws.write(1, 15, "summasi", style=centered_style)
    w = 16
    t = 19
    x = 16
    y = 17
    s = 18
    c = 19
    for con_data_type in app_models.ControlActivitiesType.objects.all():
        ws.write_merge(0, 0, w, t, "{}".format(con_data_type), style=centered_style)
        ws.write_merge(1, 1, x, y, "soni", style=centered_style)
        ws.write_merge(1, 1, s, c, "summasi", style=centered_style)
        w += 4
        t += 4
        y += 4
        x += 4
        s += 4
        c += 4

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="report.xls"'
    control_data = app_models.ControlDataFirst.objects.order_by("-id").values_list('id', 'department__name_uz', 'region__name_uz',
                                                                         'department__name_ru',
                                                                         'control_date', 'period_start',
                                                                         'period_end', 'condition_count',
                                                                         'condition_summa',
                                                                         'con_data_item__condition_count',
                                                                         'con_data_item__condition_summa')

    i, j = 2, 0
    n = 2
    m = 2
    q = 2
    k = 16
    p = 17
    d = 18
    g = 19
    for con in control_data.all():
        ws.write(i, j, '{}'.format(con[0], centered_style))
        ws.write(n, 2, "{}".format(con[1], centered_style))
        ws.write_merge(m, n, 3, 4, "{}".format(con[2], centered_style))
        ws.write_merge(m, n, 5, 6, "{}".format("MIF", centered_style))
        ws.write_merge(m, n, 7, 9, "{}".format(str(con[4]).split(" ")[0], centered_style))
        ws.write_merge(m, n, 10, 11, "{}".format(str(con[5]).split(" ")[0], centered_style))
        ws.write_merge(m, n, 12, 13, "{}".format(str(con[6]).split(" ")[0], centered_style))
        ws.write(q, 14, "{}".format(con[7], centered_style))
        ws.write(q, 15, "{}".format(con[8], centered_style))
        con_data_first = app_models.ControlDataFirst.objects.order_by("-id")
        for l in app_models.ControlDateFirstItem.objects.filter(control_date_first=con):
            for act in app_models.ControlActivitiesType.objects.values_list('id'):
                # print('act=', act)
                if l.control_activate_type.id == act[0]:
                    # print('l=', l)
                    ws.write_merge(q, q, k, p, "{}".format(l.condition_count, centered_style))
                    ws.write_merge(q, q, d, g, "{}".format(l.condition_summa, centered_style))
                    k += 4
                    p += 4
                    d += 4
                    g += 4


            # ws.write_merge(q, q, k, p, "{}".format(l.condition_count, centered_style))
            # ws.write_merge(q, q, d, g, "{}".format(l.condition_summa, centered_style))
            # k += 4
            # p += 4
            # d += 4
            # g += 4

        k = 16
        p = 17
        d = 18
        g = 19
        i += 1
        n += 1
        m += 1
        q += 1
    wb.save(response)
    return response
