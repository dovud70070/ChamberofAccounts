from django.contrib import admin
from apps.app import models as app_models
from mptt.admin import DraggableMPTTAdmin


@admin.register(app_models.Regions)
class RegionsAdmin(DraggableMPTTAdmin):
    list_display = ('id', 'tree_actions', 'indented_title', 'name_uz', 'name_ru', 'created_at', 'updated_at')
    list_display_links = ('indented_title',)


@admin.register(app_models.Departments)
class DepartmentAdmin(DraggableMPTTAdmin):
    list_display = ('id', 'tree_actions', 'indented_title', 'name_uz', 'name_ru', 'created_at', 'updated_at')
    list_display_links = ('indented_title',)
    # prepopulated_fields = {"slug": ('name_uz', 'name_ru')}


@admin.register(app_models.DepartmentType)
class DepartmentTypeAdmin(admin.ModelAdmin):
    list_display = ['id']


@admin.register(app_models.ControlActivitiesType)
class ControlActivitiesTypeAdmin(admin.ModelAdmin):
    list_display = ['id']


class ControlDateFirstItemInlineAdmin(admin.TabularInline):
    model = app_models.ControlDateFirstItem


@admin.register(app_models.ControlDataFirst)
class ControlDataAdmin(admin.ModelAdmin):
    list_display = ['id']
    inlines = [ControlDateFirstItemInlineAdmin, ]


@admin.register(app_models.ControlDateFirstItem)
class ControlDateFirstItemAdmin(admin.ModelAdmin):
    list_display = ['id', ]
