from django.urls import path, include
from apps.app import views as app_views
from django.utils.translation import gettext_lazy as _
from django.conf.urls.i18n import i18n_patterns
from apps.app import convert2excel

urlpatterns = [
    path('regions/', include([
        path('', app_views.RegionsListView.as_view(), name='regions-page'),
        path('create-region/', app_views.RegionCreateView.as_view(), name='create-region'),
        path('<int:id>/update-region/', app_views.RegionUpdateView.as_view(), name='update-region'),
        path('<int:id>/delete-region/', app_views.delete_region, name='delete-region'),
    ])),
    path('departments/', include([
        path('', app_views.DepartmentListView.as_view(), name='departments'),
        path('create-department/', app_views.DepartmentCreateView.as_view(), name='create-department'),
        path('create-department-type/', app_views.CreateDepartmentType.as_view(), name='create-department-type'),
        path('<int:id>/update-department/', app_views.DepartmentUpdateView.as_view(), name='update-department'),
        path('<int:id>/delete/', app_views.delete_department, name='delete-department')
    ])),
    path('create/con/data/', include([
        path('', app_views.control_data_view, name='control-data-table'),
        path('create-data/', app_views.CreateConData.as_view(), name='create_con_data'),
        path('detail-data/<int:id>/', app_views.control_data_detail_view, name='detail_data_view'),
        path('delete-con-data/<int:id>/', app_views.delete_con_data, name='delete-con-data')
    ])),
    # path('control-data-table/', app_views.ControlDataTabelsView.as_view(), name='control_data'),
    # path('download', app_views.download_csv, name='download_csv'),
    path('control-data-table/', include([
        path('', app_views.ControlDataTableView.as_view(), name='control_data'),
    ])),
    path('control-activities/', include([
        path('', app_views.control_activity, name='control-activities'),
        path('create-activity/', app_views.ActivityCreateView.as_view(), name='create-activity'),
        path('update-activity/<int:id>/', app_views.ActivityUpdateView.as_view(), name='update-activity'),
        path('delete-activity/<int:id>/', app_views.delete_activity, name='delete-activity')
    ])),
    path('get-excel/', convert2excel.excel_view, name='get-excel')
]
