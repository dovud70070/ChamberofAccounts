from django.contrib import admin
from apps.account import models as account_models
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.text import gettext_lazy as _


@admin.register(account_models.CustomUser)
class UserAdmin(DjangoUserAdmin):
    fieldsets = (
        (_('Main'), {'fields': ('username', 'password')}),
        (_('Personal info'),
         {'fields': ('first_name', 'last_name', 'phone', 'email', 'type_of_user', "phone_verification_code", 'image')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
            'username', 'email', 'password1', 'password2', 'type_of_user', 'phone', 'dob', 'first_name', 'last_name',
            'image'),
        }),
    )
    list_display = ('get_fullname', 'id', 'type_of_user', 'is_staff', 'date_joined',)
    search_fields = ('email', 'first_name', 'last_name', 'phone',)
    ordering = ('email',)
    list_per_page = 200

    @staticmethod
    def get_fullname(obj):
        if obj.first_name or obj.last_name:
            return "{} {}".format(obj.first_name, obj.last_name)
        return "{}".format(obj.username)


@admin.register(account_models.Permissions)
class PermissionsAdmin(admin.ModelAdmin):
    list_display = ['id']


@admin.register(account_models.Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ['id']


@admin.register(account_models.Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'user']
