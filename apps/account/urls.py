from django.urls import path
from .views import logoutView, login, RegisterView, UserPages, Home, Profile, UserUpdateView
from django.urls import path, include
from .views import logoutView, login, RegisterView

urlpatterns = [
    # path('login/', login, name='login-page'),
    path('profile/', Profile.as_view(), name='profile-page'),
    path('home/', Home.as_view(), name='home-page'),
    path('', login, name='login-page'),
    path('logout/', logoutView, name='logout'),
    path('user-create/', RegisterView.as_view(), name='user-create'),
    path('users/', UserPages.as_view(), name='users-page'),
    path('<int:pk>/', UserUpdateView.as_view(), name='user-update'),

]

