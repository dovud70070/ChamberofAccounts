from django.template.loader import render_to_string


def get_header(request):
    return render_to_string('parts/header.html', {
        'request': request
    })

def get_footer(request):
    return render_to_string('parts/foter.html', {
        'request': request
    })




def standart(request):
    context = dict()
    context['header'] = get_header(request)
    context['footer'] = get_footer(request)
    return context
